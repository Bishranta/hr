package com.infodev.hr.model;

public class RelativeDetail {

	private Integer id;
	private Integer employeeId;
	private String firstName;
	private String lastName;
	private String relationship;
	private String citizenshipNumber;
	private Integer contactNumber;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCitizenshipNumber() {
		return citizenshipNumber;
	}

	public void setCitizenshipNumber(String citizenshipNumber) {
		this.citizenshipNumber = citizenshipNumber;
	}

	public Integer getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Integer contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public RelativeDetail() {

	}

	public RelativeDetail(Integer id, Integer employeeId, String firstName, String lastName, String relationship,
			String citizenshipNumber, Integer contactNumber) {
		this.id = id;
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.citizenshipNumber = citizenshipNumber;
		this.contactNumber = contactNumber;
		this.relationship = relationship;

	}

}
