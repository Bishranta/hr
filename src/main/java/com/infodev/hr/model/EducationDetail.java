package com.infodev.hr.model;

import java.util.Date;

public class EducationDetail {
	private Integer id;
	private Integer employeeId;
	private String boardName;
	private String passedLevel;
	private Date passedYear;
	private String obtainedDivision;
	private Float obtainedPercentage;

	public EducationDetail() {

	}

	public EducationDetail(Integer id, Integer employeeId, String boardName, String passedLevel, Date passedYear,
			String obtainedDivision, Float obtainedPercentage) {
		this.id = id;
		this.employeeId = employeeId;
		this.boardName = boardName;
		this.passedLevel = passedLevel;
		this.passedYear = passedYear;
		this.obtainedDivision = obtainedDivision;
		this.obtainedPercentage = obtainedPercentage;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getPassedLevel() {
		return passedLevel;
	}

	public void setPassedLevel(String passedLevel) {
		this.passedLevel = passedLevel;
	}

	public Date getPassedYear() {
		return passedYear;
	}

	public void setPassedYear(Date passedYear) {
		this.passedYear = passedYear;
	}

	public String getObtsinedDivision() {
		return obtainedDivision;
	}

	public void setObtainedDivision(String obtainedDivision) {
		this.obtainedDivision = obtainedDivision;
	}

	public Float getObtainedPercentage() {
		return obtainedPercentage;
	}

	public void setObtainedPercentage(Float ObtainedPercentage) {
		this.obtainedPercentage = obtainedPercentage;
	}

}
