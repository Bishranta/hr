package com.infodev.hr.model;

import java.util.Date;
import java.util.List;

public class Employee {
	private Integer id;
	private String firstName;
	private String lastName;
	private Double salary;
	private boolean isActive;
	private String citizenshipNumber;
	private Date dateOfBirth;

	private List<EducationDetail> educationDetails;
	private List<RelativeDetail> relativeDetails;

	public Employee() {

	}

	public Employee(Integer id, String firstName, String lastName, Double salary, boolean isActive,
			String citizenshipNumber, Date dateOfBirth) {
		this.id = id;
		this.dateOfBirth = dateOfBirth;
		this.citizenshipNumber = citizenshipNumber;
		this.isActive = isActive;
		this.lastName = lastName;
		this.firstName = firstName;
		this.salary = salary;

	}

	public List getEducationDetails() {
		return educationDetails;
	}

	public void setEducationDetails(List<EducationDetail> educationDetails) {
		this.educationDetails = educationDetails;
	}

	public List getRelativeDetails() {
		return relativeDetails;
	}

	public void setRelativeDetails(List<RelativeDetail> relativeDetails) {
		this.relativeDetails = relativeDetails;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public boolean getisActive() {
		return isActive;
	}

	public void setisActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCitizenshipNmuber() {
		return citizenshipNumber;
	}

	public void setCitizenshipNumber(String citizenshipNumber) {
		this.citizenshipNumber = citizenshipNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
