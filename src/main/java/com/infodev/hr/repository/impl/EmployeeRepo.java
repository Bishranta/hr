package com.infodev.hr.repository.impl;

import java.util.List;

import com.infodev.hr.model.Employee;


public interface EmployeeRepo {
	Employee insert (Employee employee);
	List<Employee> findAll();
	Integer findMax();
	
	
}
