package com.infodev.hr.repository.impl;

import java.util.List;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.Employee;

public interface EducationDetailRepo {
    
	EducationDetail insert (EducationDetail educationDetail);
	List<EducationDetail> findAllEducationDetail(Employee employee);
	
	List<EducationDetail> findByEmployeeId(Integer employeeId);
	
	
	
	
}
