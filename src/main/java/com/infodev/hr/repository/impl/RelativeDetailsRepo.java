package com.infodev.hr.repository.impl;

import java.util.List;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.RelativeDetail;

public interface RelativeDetailsRepo {
     RelativeDetail insert(RelativeDetail relativeDetails);
	 List <RelativeDetail> findAllRelativeDetail();
	 
	 List<RelativeDetail> findByEmployeeId(Integer employeeId);
		
	
	
	
	
	
	
	
}
