package com.infodev.hr.repository.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.RelativeDetail;
import com.infodev.hr.repository.impl.RelativeDetailsRepo;
@Repository
public class RelativeRepoImpl  implements RelativeDetailsRepo{
	@Autowired
 private JdbcTemplate jdbcTemplate;
 
	@Override
	public RelativeDetail insert(RelativeDetail relativeDetails) {
		// TODO Auto-generated method stub
		jdbcTemplate.update("insert into employee_relatives values(?,?,?,?,?,?,?)", 
				relativeDetails.getId(),relativeDetails.getEmployeeId(),
				relativeDetails.getFirstName(),relativeDetails.getLastName(),
				relativeDetails.getRelationship(),relativeDetails.getCitizenshipNumber(),
				relativeDetails.getContactNumber());
		return relativeDetails;
	}

	@Override
	public List<RelativeDetail> findAllRelativeDetail() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("selecet id, employee_id,first_name,last_name,"
				+ "relationship,citizenship_number,contact_number",(rs,rowNum) -> new RelativeDetail(rs.getInt("id"),
						rs.getInt("employee_id"), rs.getString("first_name"),rs.getString("last_name"),
						rs.getString("relationship"),rs.getString("citizenship_number"),
					rs.getInt("contact_number")	));
				
	}

	@Override
	public List<RelativeDetail> findByEmployeeId(Integer employeeId) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select id, employee_id,first_name, last_name,relationship,citizenship_number,contact_number from employee_relatives where employee_id= ?"
				,new Object[] {employeeId},
(rs, rowNum) -> new RelativeDetail(rs.getInt("id"),rs.getInt("employee_id"), 
		rs.getString("first_name"), rs.getString("last_name"), rs.getString("relationship"),
		rs.getString("citizenship_number"),
		rs.getInt("contact_number")));
	}
	    

}
