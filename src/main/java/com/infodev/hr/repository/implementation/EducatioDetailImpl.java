package com.infodev.hr.repository.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.Employee;
import com.infodev.hr.repository.impl.EducationDetailRepo;

@Repository
public class EducatioDetailImpl implements EducationDetailRepo {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public EducationDetail insert(EducationDetail educationDetail) {
		// TODO Auto-generated method stub
		jdbcTemplate.update("insert into education_details values(?,?,?,?,?,?,?)", educationDetail.getId(),
				educationDetail.getEmployeeId(), educationDetail.getBoardName(), 
				educationDetail.getPassedLevel(),
				educationDetail.getPassedYear(), educationDetail.getObtsinedDivision(),
				educationDetail.getObtainedPercentage());

		return educationDetail;
	}

	@Override
	public List<EducationDetail> findAllEducationDetail(Employee employee) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select id, employee_id, board_name, passed_level, passed_year, obtained_division,"
				+ "obtained_percentage ",(rs, rowNum) -> new EducationDetail(rs.getInt("id"), 
						rs.getInt("employee_id"),rs.getString("board_name"),rs.getString("passed_level"),
						rs.getDate("passed_year"),rs.getString("obtained_division"),
						rs.getFloat("obtained_percentage")));
				
				
				
			
	}

	@SuppressWarnings("deprecation")
	@Override
	
	public List<EducationDetail> findByEmployeeId(Integer employeeId) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select id, employee_id, board_name, passed_level, passed_year, obtained_divison,obtained_percentage from education_details where employee_id= ?"
								,new Object[] {employeeId},
				(rs, rowNum) -> new EducationDetail(rs.getInt("id"), 
						rs.getInt("id"),rs.getString("board_name"),
						rs.getString("passed_level"),
					rs.getDate("passed_year"),rs.getString("obtained_divison"),
					rs.getFloat("obtained_percentage")));	
	
	}
	

	
	
	

}
