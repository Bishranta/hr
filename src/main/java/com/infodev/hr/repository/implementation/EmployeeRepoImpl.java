package com.infodev.hr.repository.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.infodev.hr.model.Employee;
import com.infodev.hr.repository.impl.EmployeeRepo;

@Repository
public class EmployeeRepoImpl implements EmployeeRepo {

	// Spring Boot will create and configure DataSource and JdbcTemplate
	// To use it, just use @Autowired
	@Autowired

	private JdbcTemplate jdbcTemplate;

	@Override
	public Employee insert(Employee employee) {
		// TODO Auto-generated method stub
		jdbcTemplate.update("insert into employee values(?,?,?,?,?,?,?)", employee.getFirstName(),
				employee.getLastName(), employee.getSalary(), employee.getisActive(), employee.getCitizenshipNmuber(),
				employee.getDateOfBirth(), employee.getId());
		return employee;
	}

	@Override
	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query(
				"select id, first_name, last_name, salary,"
						+ "isActive ,citizenship_number, date_of_birth  from employee",
				(rs, rowNum) -> new Employee(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"),
						rs.getDouble("salary"), rs.getBoolean("isActive"), rs.getString("citizenship_number"),
						rs.getDate("date_of_birth")));

	}

	@Override
	public Integer findMax() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("selcet max(id) from employee", null);
	}

}
