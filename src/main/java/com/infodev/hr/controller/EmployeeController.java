package com.infodev.hr.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infodev.hr.model.Employee;
import com.infodev.hr.service.HrService;

@RestController
@RequestMapping("employee")

public class EmployeeController {
	private final HrService hrService;

	public EmployeeController(HrService hrService) {
		this.hrService = hrService;
	}

	@PostMapping("/save-employee")
	public ResponseEntity saveEmployee(@RequestBody Employee employee) {
		// System.out.println("Hello ");
		hrService.saveEmployee(employee);
		return new ResponseEntity("Inserted Suceessfully", HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity findAllEmployee() {
		return new ResponseEntity(hrService.findAllEmployee(), HttpStatus.OK);
	}
	

//	@GetMapping("/find-by-employee-id/{id}")
//	public ResponseEntity findByEmployeeId(@PathVariable Integer id) {
//		return new ResponseEntity(hrService.findbyEmployeeId(id), HttpStatus.OK);
//	}

}
