package com.infodev.hr.service;

import java.util.List;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.Employee;
import com.infodev.hr.model.RelativeDetail;

public interface HrService {
     void saveEmployee(Employee employee);
     List<Employee> findAllEmployee();
     
//     List<EducationDetail> findbyEmployeeId(Integer id);
//        List<RelativeDetail>findByEmployeeId(Integer id);
        
}
