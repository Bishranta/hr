package com.infodev.hr.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.infodev.hr.model.EducationDetail;
import com.infodev.hr.model.Employee;
import com.infodev.hr.model.RelativeDetail;
import com.infodev.hr.repository.impl.EducationDetailRepo;
import com.infodev.hr.repository.impl.EmployeeRepo;
import com.infodev.hr.repository.impl.RelativeDetailsRepo;

@Service
public class HrServiceImpl implements HrService {
	
private  final EmployeeRepo employeeRepo;
private final EducationDetailRepo eduDetailRepo;
private final  RelativeDetailsRepo relativeDetailsRepo;
     
        public HrServiceImpl(EmployeeRepo employeeRepo,EducationDetailRepo eduDetailRepo,RelativeDetailsRepo relativeDetailsRepo) {
        	this.employeeRepo=employeeRepo;
        	this.eduDetailRepo=eduDetailRepo;
        	this.relativeDetailsRepo=relativeDetailsRepo;
        }

		@Override
		public void saveEmployee(Employee employee) {
			// TODO Auto-generated method stub
			employeeRepo.insert(employee);
			   
			
			List<EducationDetail> educationDetails= employee.getEducationDetails();
			List<RelativeDetail> relativeDetails= employee.getRelativeDetails();
			
			for(EducationDetail educationDetail:educationDetails) {
				eduDetailRepo.insert(educationDetail);      
			}
			
			for(RelativeDetail relativeDetail:relativeDetails) {
				relativeDetailsRepo.insert(relativeDetail);
			}
			
			
//			return employeeRepo.insert(employee);
		}

		@Override
		public List<Employee> findAllEmployee() {
			// TODO Auto-generated method stub
		      
			List<Employee>employees = employeeRepo.findAll();
			
			for(Employee employee:employees) {
				//System.out.println(employee.getId());
			employee.setEducationDetails(eduDetailRepo.findByEmployeeId(employee.getId()));
			employee.setRelativeDetails(relativeDetailsRepo.findByEmployeeId(employee.getId())); 
			}
			
		     
			   
			
			
		 return employees;
			
			
		}

//		@Override
//		public List<EducationDetail> findbyEmployeeId(Integer id) {
//			// TODO Auto-generated method stub
//			return eduDetailRepo.findByEmployeeId(id);
//		}

		
		  
		
		

//		@Override
//		public EducationDetail saveEducationDetail(EducationDetail educationDetail) {
//			// TODO Auto-generated method stub
//			return educationDetailRepo.insert(educationDetail);
//		}
////
//	@Override
//	public RelativeDetail saveRelativeDetails(RelativeDetail relativeDetails) {
//		// TODO Auto-generated method stub
//		return relativeDetailsRepo.insert(relativeDetails);
//	}
////	      
//        
        
        
        
	
	
}
